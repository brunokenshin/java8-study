package br.com.alura;

import static java.util.Comparator.comparing;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class Java8Test {
	
	private List<String> palavras;
	private List<Curso> cursos;
	
	@BeforeEach
	public void beforeTest() {
		palavras = Arrays.asList("alura online","casa do codigo","caelum");
		cursos = Arrays.asList(new Curso("Python", 45), 
							   new Curso("JavaScript", 150), 
							   new Curso("Java 8", 113), 
							   new Curso("C", 55));
	}

//	Exercício 1:
	@Test
	public void test_foreach_with_consumer_default_method() {
		Consumer<String> printLineConsumer = new PrintLineConsumer();
		palavras.forEach(printLineConsumer);
	}

	class PrintLineConsumer implements Consumer<String> {
		@Override
		public void accept(String string) {
			System.out.println(string);
		}
	}
	
//	=========================================================================
	
//	Exercício 2:
	@Test
	public void test_sort_with_comparator_default_method() {
		Comparator<String> sizeComparator = new SizeComparator();
		palavras.sort(sizeComparator);
		
		assertEquals("caelum", palavras.get(0));
		assertEquals("alura online", palavras.get(1));
		assertEquals("casa do codigo", palavras.get(2));
	}
	
	class SizeComparator implements Comparator<String> {
		@Override
		public int compare(String string1, String string2) {
			if (string1.length() > string2.length()) {
				return 1;
			}
			
			if (string1.length() < string2.length()) {
				return -1;
			}
			return 0;
		}
	}

//	=========================================================================
	
//	Exercício 3:
	@Test
	public void test_foreach_with_lambda() {
		palavras.forEach(palavra -> System.out.println(palavra));
	}
	
//	=========================================================================
	
//	Exercício 4:
	@Test
	public void test_sort_with_lambda() {
		palavras.sort((palavra1, palavra2) -> Integer.compare(palavra1.length(), palavra2.length()));
		
		assertEquals("caelum", palavras.get(0));
		assertEquals("alura online", palavras.get(1));
		assertEquals("casa do codigo", palavras.get(2));
	}
	
//	=========================================================================
	
//	Exercício 5:
	@Test
	public void test_runnable_with_lambda() {
		new Thread(()-> System.out.println("Executando um Runnable")).start();
	}
	
//	=========================================================================
	
//	Exercício 6:
	@Test
	public void test_sort_with_comparator_default_method_and_lambda() {
		palavras.sort(Comparator.comparing(s -> s.length()));
		
		assertEquals("caelum", palavras.get(0));
		assertEquals("alura online", palavras.get(1));
		assertEquals("casa do codigo", palavras.get(2));
	}
	
//	=========================================================================
	
//	Exercício 7:
	@Test
	public void test_sort_with_comparator_method_reference() {
		palavras.sort(Comparator.comparing(String::length));
		
		assertEquals("caelum", palavras.get(0));
		assertEquals("alura online", palavras.get(1));
		assertEquals("casa do codigo", palavras.get(2));
	}
	
//	=========================================================================
	
//	Exercício 8:
	@Test
	public void test_sort_with_comparator_method_reference_and_static_import() {
		palavras.sort(comparing(String::length));
		
		assertEquals("caelum", palavras.get(0));
		assertEquals("alura online", palavras.get(1));
		assertEquals("casa do codigo", palavras.get(2));
	}
	
//	=========================================================================
	
//	Exercício 9:
	@Test
	public void test_sort_with_another_comparator() {
		palavras.sort(String.CASE_INSENSITIVE_ORDER);
		
		assertEquals("alura online", palavras.get(0));
		assertEquals("caelum", palavras.get(1));
		assertEquals("casa do codigo", palavras.get(2));
	}
	
//	=========================================================================
	
//	Exercício 10:
	@Test
	public void test_foreach_with_method_reference() {
		palavras.forEach(System.out::println);
	}
	
//	=========================================================================
	
//	Exercício 11:
	@Test
	public void test_sort_by_attribute_with_reference_method_or_labmda() {
//		cursos.sort(Comparator.comparingInt(curso -> curso.getAlunos()));
//		OR
//
		cursos.sort(Comparator.comparingInt(Curso::getAlunos));
		
		assertEquals("Python", cursos.get(0).getNome());
		assertEquals("C", cursos.get(1).getNome());
		assertEquals("Java 8", cursos.get(2).getNome());
		assertEquals("JavaScript", cursos.get(3).getNome());
	}
	
//	=========================================================================
	
//	Exercício 12:
	@Test
	public void test_stream_with_filter() {
//		cursos.stream()
//				.filter(curso -> curso.getAlunos() > 50)
//				.forEach(curso -> System.out.println(curso.getNome()));
//
//		OR
//		
		cursos.stream()
				.filter(curso -> curso.getAlunos() > 50)
				.map(Curso::getNome) //transforma em uma stream de nomes de curso
				.forEach(System.out::println);
	}
	
//	=========================================================================
	
//	Exercício 13:
	@Test
	public void test_refactor_to_use_method_reference() throws Exception {
//		Refactor this code to use method reference:
//
//		cursos.stream()
//				.filter(c -> c.getAlunos() > 50)
//				.map(c -> c.getAlunos())
//				.forEach(x -> System.out.println(x));
		
		cursos.stream()
				.filter(c -> c.getAlunos() > 50)
				.map(Curso::getAlunos)
				.forEach(System.out::println);
	}
	
//	=========================================================================
	
//	Exercício 14:
	@Test
	public void test_get_first_of_stream() {
		Optional<Curso> curso = cursos.stream().filter(c -> c.getAlunos() > 50).findFirst();
		assertEquals("JavaScript", curso.get().getNome());
	}

//	=========================================================================
	
//	Exercício 15:
	@Test
	public void test_get_avarage_of_values_with_streams() {
		Double average = cursos.stream()
								.mapToInt(Curso::getAlunos)
								.average().orElse(1);

		assertEquals(Double.valueOf(90.75), average);
	}
	
//	=========================================================================
	
//	Exercício 16:
	@Test
	public void test_collect_stream_to_list() {
//		Transform this stream to a list
//		Stream<Curso> stream = cursos.stream().filter(c -> c.getAlunos() > 50);
		
		List<Curso> list = cursos.stream()
								 .filter(c -> c.getAlunos() > 50)
								 .collect(Collectors.toList());
		
		assertEquals("JavaScript", list.get(0).getNome());
		assertEquals("Java 8", list.get(1).getNome());
		assertEquals("C", list.get(2).getNome());
	}
	
//	=========================================================================
	
//	Exercício 17:
	@Test
	public void test_print_date_time() {
		LocalDate today = LocalDate.now();
		
		System.out.println(today);
	}
	
//	=========================================================================
	
//	Exercício 18:	
	@Test
	public void test_create_dates_with_specific_values() {
		LocalDate date = LocalDate.of(2099, Month.JANUARY, 25); 
//		OR
		LocalDate date2 = LocalDate.of(2099, 1, 25);
		
		assertEquals(date, date2);
	}

//	=========================================================================
	
//	Exercício 19:
	@Test
	public void test_check_period_between_two_dates() {
		LocalDate dateWhenIWriteThis = LocalDate.of(2018, Month.MARCH, 28);
		LocalDate futureDate = LocalDate.of(2099, Month.JANUARY, 25);
		
		Period period = Period.between(dateWhenIWriteThis, futureDate);

		assertEquals(80, period.getYears());
		assertEquals(9, period.getMonths());
		assertEquals(28, period.getDays());
	}
	
//	=========================================================================
	
//	Exercício 20:
	@Test
	public void test_format_dates() {		
		LocalDate date = LocalDate.of(2018, Month.MARCH, 28);
		DateTimeFormatter brazilianFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		DateTimeFormatter isoDateFormatter = DateTimeFormatter.ISO_DATE;
		
		assertEquals("28/03/2018", date.format(brazilianFormatter));
		assertEquals("2018-03-28", date.format(isoDateFormatter));
	}
	
}